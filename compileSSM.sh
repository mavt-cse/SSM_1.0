#! /bin/csh -f
#
# 
# 
########## Set these paths to the base directories of the Blitz++, SBML, and BOOST libraries ############
set BLITZ_PATH=/Users/basil/Desktop/SSM-Libs/blitz-0.9/
set SBML_PATH=/Users/basil/Desktop/SSM-Libs/libsbml-2.3.4/
set BOOST_PATH=/Users/basil/Desktop/SSM-Libs/boost_1_37_0/
#########################################################################################################

echo --------------------------------------------------------------------------------------------------------------
echo ---------------------------------COMPILATION--ERRORS-AND-WARNINGS---------------------------------------------
echo --------------------------------------------------------------------------------------------------------------

g++ -O3 -I${BOOST_PATH} -I${BLITZ_PATH}/include/blitz -I${SBML_PATH}/include/sbml -c ./Code/RNGLib/*.cpp ./Code/Timer/*.cpp ./Code/SBMLReaderAndParser.cpp ./Code/Simulation.cpp ./Code/SSMReaction.cpp ./Code/StochasticSimulationMethods.cpp ./Code/Methods/*.cpp

echo --------------------------------------------------------------------------------------------------------------
echo --------------------------------------------------------------------------------------------------------------

echo COMPILE_SCRIPT_FINISHED

echo --------------------------------------------------------------------------------------------------------------
echo --------------------------------------------------------------------------------------------------------------





