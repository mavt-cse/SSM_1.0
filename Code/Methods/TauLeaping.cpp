/*
 *  TauLeaping.cpp
 *  StochasticSimulationMethods
 *
 *  Created by Basil Bayati on 10/7/08.
 *  Copyright 2008 Basil Bayati. All rights reserved.
 *
 */

#include "TauLeaping.h"

TauLeaping::TauLeaping(Simulation * simulation):
Method(simulation)
{
}

TauLeaping::~TauLeaping()
{
}

double TauLeaping::computeTimeStep()
{
	double tauPrime;
	
	double epsilon	= simulation->Epsilon;
		
	int numberOfSpecies		= sbmlModel->getNumSpecies();
	Array<int, 1> hor			(numberOfSpecies);
	Array<int, 1> nuHor			(numberOfSpecies);
	Array<double, 1> muHat		(numberOfSpecies);
	Array<double, 1> sigmaHat2	(numberOfSpecies);
	Array<double, 1> varHat		(numberOfSpecies);
	hor = 0; nuHor = 0; muHat = 0.0; sigmaHat2 = 0.0;
	
	computeHor(hor, nuHor);
	computeMuHatSigmaHat2(muHat, sigmaHat2);
	
	double tau, taup,  epsi, epsixi, epsixisq;
	double xi;
	
	tau = HUGE_VAL;
	
	double a0 = (double)blitz::sum(propensitiesVector);
	for (int is = 0; is < numberOfSpecies; is++)
	{
		varHat(is) = sigmaHat2(is) - (1.0/a0) * muHat(is) * muHat(is);
	}
				
	for (int is = 0; is < numberOfSpecies; ++is)
	{
		taup = (HUGE_VALF*0.5);
		xi = (double)simulation->speciesValues(is);
		switch (hor(is)) {
			case 0:
				break;
			case 1:
				epsi = epsilon;
				epsixi = epsi * xi;
				epsixi = max(epsixi,1.0);
				tau = min(tau,epsixi/fabsf(muHat(is)));
				epsixisq = epsixi*epsixi;
				tau = min(tau,epsixisq/varHat(is));
				break;
			case 2:
				if (nuHor(is) == 1)
					epsi = 0.5*epsilon;
				else
					epsi = epsilon*(xi-1.0)/(2.0*(xi-1.0)+1.0);
				epsixi = epsi * xi;
				epsixi = max(epsixi,1.0);
				tau = min(tau,epsixi/fabs(muHat(is)));
				epsixisq = epsixi*epsixi;
				tau = min(tau,epsixisq/varHat(is));
				break;
			case 3:
				if (nuHor(is)==1)
					epsi = 0.3333333333*epsilon;
				else if (nuHor(is) == 2)
					epsi = epsilon*(xi-1)/(3.0*(xi-1)+1.5);
				else
					epsi = epsilon*(xi-1)*(xi-2)/(3.0*(xi-1)*(xi-2)+(xi-2)+2.0*(xi-1));
				epsixi = epsi * xi;
				epsixi = max(epsixi,1.0);
				tau = min(tau,epsixi/fabsf(muHat(is)));
				epsixisq = epsixi*epsixi;
				tau = min(tau,epsixisq/varHat(is));
				break;
			default:
				break;
		}
	}
	tauPrime = tau;
	return tauPrime;
}

void TauLeaping::solve()
{
	cout << "TauLeaping..." << endl;
	
	double aj;
	long int kj;
	double a0 = 0.0;
	double dt;
	bool isNegative = false;
	
	openAuxiliaryStream( (simulation->ModelName) + "-histogram-tauLeaping.txt");
	
	for (int samples = 0; samples < numberOfSamples; ++samples)
	{
		t = simulation->StartTime;
		numberOfIterations = 0;
		timePoint = 0;
		simulation->loadInitialConditions();
		
		while (t < tEnd)
		{
			saveData();
			computePropensities(propensitiesVector, 0);
			a0 = blitz::sum(propensitiesVector);
	
			if (isNegative == false)
			{
				dt = computeTimeStep();
			}
			
			for (int j = 0; j < propensitiesVector.extent(firstDim); ++j)
			{				
				aj				= propensitiesVector(j);
				kj				= ignpoi( aj*dt );
				
				fireReactionProposed( j , kj );
			}

			if (isProposedNegative() == false)
			{
				acceptNewSpeciesValues();
				++numberOfIterations;
				t += dt;
				isNegative = false;
			}
			else
			{
				cout << "Negative species at time: " << t << endl;
				dt = dt / 2.0;
				reloadProposedSpeciesValues();
				isNegative = true;
			}			
		}
		
		saveData();
		cout << "Sample: " << samples << endl;
		writeToAuxiliaryStream( simulation->speciesValues );
	}
	writeData(outputFileName);
	closeAuxiliaryStream();
}

