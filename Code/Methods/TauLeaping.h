/*
 *  TauLeaping.h
 *  StochasticSimulationMethods
 *
 *  Created by Basil Bayati on 10/7/08.
 *  Copyright 2008 Basil Bayati. All rights reserved.
 *
 */

#pragma once

#include "../HeaderFiles.h"
#include "../Method.h"

class TauLeaping : public Method
{
public:
	TauLeaping(Simulation * simulation);
	~TauLeaping();
	
	// override the virtual method
	void solve();
private:
	double computeTimeStep();
};

