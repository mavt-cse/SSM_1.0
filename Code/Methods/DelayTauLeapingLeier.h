/*
 *  DelayTauLeapingLeier.h
 *  StochasticSimulationMethods
 *
 *  Created by Basil Bayati on 10/7/08.
 *  Copyright 2008 Basil Bayati. All rights reserved.
 *
 */

#pragma once

#include "../HeaderFiles.h"
#include "../Method.h"

class DelayTauLeapingLeier : public Method
{
public:
	DelayTauLeapingLeier(Simulation * simulation);
	~DelayTauLeapingLeier();
		
	// override the virtual method
	void solve();
private:

};

