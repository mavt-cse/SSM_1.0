/*
 *  StochasticSimulationMethods.cpp
 *  StochasticSimulationMethods
 *
 *  Created by Basil Bayati on 5/5/08.
 *  Copyright 2008 Basil Bayati. All rights reserved.
 *
 */

// C/C++, Blitz++, RanLib, SBML, and Boost

#include "HeaderFiles.h"

	// Program Files
	#include "SBMLReaderAndParser.h"
	#include "Simulation.h"

	#define PARTICLEDEFINED
	#include "Method.h"
	
	#include "Timer/Timer.h"

		// Simulation Methods
		#include "Methods/SSA.h"
		
		#include "Methods/SSALDM.h"

		#include "Methods/RLeaping.h"
		#include "Methods/TauLeaping.h"
		
		#include "Methods/DelaySSA.h"
		#include "Methods/DelayRLeaping.h"
		#include "Methods/DelayTauLeaping.h"
		#include "Methods/DelayTauLeapingLeier.h"
		#include "Methods/DelayTauLeapingPhilippe.h"


int main (int argc, char * const argv[]) 
{
	// You must supply a SBML file
	if (argc < 2)
	{
		cout << endl << "Oh blimey.  Evidently something has gone askew old chap." << endl;
		cout << "Do check that you have supplied an input file via the command line." << endl;
		return EXIT_FAILURE;
	}
	
	string filename = argv[1];
	
	if (argc >= 4)
	{
		setall( (long int) atof(argv[2]), (long int) atof(argv[3]) ); // RNG seeds
		cout << "Random Number Generator Seeds Set." << endl << endl;
	}
	else
	{
		setall(1056577, 112025); // RNG seeds
	}
	
	SBMLReaderAndParser * sBMLReaderAndParser	= new SBMLReaderAndParser(filename);
	sBMLReaderAndParser->readAndParse();
	SBMLDocument * sbmlDocument					= sBMLReaderAndParser->getSBMLDocument();
	
	cout << "Are you content with the parsing of the SBML file? (y/n)" << endl;
	string inputString;
	cin  >> inputString;
	string toSimulateOrNotToSimulate = boost::algorithm::trim_copy( inputString );
	
	if ( (toSimulateOrNotToSimulate != "y") && (toSimulateOrNotToSimulate != "Y") && (toSimulateOrNotToSimulate != "yes") && (toSimulateOrNotToSimulate != "YES") )
	{
		cout << "Since you are discontent, the process will terminate.  Do review your SBML input file." << endl;
		delete sBMLReaderAndParser;
		return EXIT_SUCCESS;
	}
					
	Simulation	* simulation	= new Simulation(sbmlDocument);	
	Method		* method		= NULL;
	
	string selectedMethod = boost::algorithm::trim_copy( simulation->StochasticSimulationMethod );
	
	if ( ( selectedMethod == "SSA")				|| ( selectedMethod == "ESSA")				|| ( selectedMethod == "StochasticSimulationAlgorithm") )
	{ method	= new SSA		(simulation); }
	else if ( ( selectedMethod == "SSALDM")		|| ( selectedMethod == "SSA-LDM")			|| ( selectedMethod == "SSA-LogDM")		|| ( selectedMethod == "SSA-LogDirectMethod") )
	{ method	= new SSALDM	(simulation); }
	else if ( ( selectedMethod == "DSSA")		|| ( selectedMethod == "DelaySSA")			|| ( selectedMethod == "D-SSA")			|| ( selectedMethod == "Delay-SSA") )
	{ method	= new DelaySSA	(simulation); }
	else if ( ( selectedMethod == "RLeap")		|| ( selectedMethod == "RLeaping")			|| ( selectedMethod == "R-Leap")		|| ( selectedMethod == "R-Leaping") )
	{ method	= new RLeaping	(simulation); }
	else if ( ( selectedMethod == "TauLeap")	|| ( selectedMethod == "TauLeaping")		|| ( selectedMethod == "Tau-Leap")		|| ( selectedMethod == "Tau-Leaping") )
	{ method	= new TauLeaping (simulation); }
	else if ( ( selectedMethod == "DRLeap")		|| ( selectedMethod == "DelayRLeaping")		|| ( selectedMethod == "DR-Leap")		|| ( selectedMethod == "DR-Leaping") )
	{ method	= new DelayRLeaping (simulation); }
	else if ( ( selectedMethod == "DTauLeap")	|| ( selectedMethod == "DelayTauLeaping")	|| ( selectedMethod == "DTau-Leap")		|| ( selectedMethod == "DTau-Leaping") )
	{ method	= new DelayTauLeapingPhilippe (simulation); }
	else if ( ( selectedMethod == "DTauLeapLeier")	|| ( selectedMethod == "DelayTauLeapingLeier")	|| ( selectedMethod == "DTau-LeapLeier")		|| ( selectedMethod == "DTau-LeapingLeier") )
	{ method	= new DelayTauLeapingLeier	  (simulation); }
	else if ( ( selectedMethod == "DRLeap")		|| ( selectedMethod == "DelayRLeaping")		|| ( selectedMethod == "DR-Leap")		|| ( selectedMethod == "DR-Leaping") )
	{ method	= new DelayRLeaping  (simulation); }
	else
	{ 
		cout << "Oh blimey.  The simulation method that you requested, \"" << selectedMethod << "\", is not yet supported." << endl; 
	}
	
	if ( method != NULL) {
	Timer timer;
	timer.StartSW();
	method->solve(); 
	timer.StopSW();
	cout << "Running time: " << timer.ReadSW() << endl;
	}

	delete method;
	delete simulation;
	delete sBMLReaderAndParser;
	return EXIT_SUCCESS;
}

