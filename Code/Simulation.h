/*
 *  Simulation.h
 *  StochasticSimulationMethods
 *
 *  Created by Basil Bayati on 5/5/08.
 *  Copyright 2008 Basil Bayati. All rights reserved.
 *
 */
 
#pragma once
#include "HeaderFiles.h"
#include "SSMReaction.h"

class Simulation
{
public:

	Simulation (SBMLDocument * sbmlDocument);
	Simulation ();
	~Simulation();

	Model			* getSBMLModel();
	int				  getSpeciesIndex(string speciesName);
	void			  loadInitialConditions();
	
	Array< ParticleType, 1 > speciesValues;
	Array< ParticleType, 1 > proposedSpeciesValues;
	Array<   double    , 2 > speciesEnsemble;
	
	vector<SSMReaction* >	ssmReactionList;
	vector < vector <int> > backPointers;  // matrix, (species) x (reactions in which the speices is involved)
	
	// initial values for the simulation
	double StartTime, EndTime, Epsilon, Theta;
	int    NumberOfSamples, StoreInterval, SortInterval;
	
	string StochasticSimulationMethod;
	string ModelName;
	
private:

	void		parseAnnotation();
	double		dFindSubstringAnnotation(string annotation, string s [2]);
	string		sFindSubstringAnnotation(string annotation, string s [2]);

	map< string, int >				speciesData;
	map< string, int >::iterator	iter;
	
	
	SBMLDocument	*	sbmlDocument;
	Model			*	sbmlModel;
	Reaction		*	sbmlReaction;
	Species			*	sbmlSpecies;
	
	string timeStart		[2];
	string timeEnd			[2];
	string storeInterval	[2];
	string epsilon			[2];
	string numberOfSamples	[2];
	string method			[2];
	string theta			[2];
	string sortInterval		[2];
};
