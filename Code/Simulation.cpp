/*
 *  Simulation.cpp
 *  StochasticSimulationMethods
 *
 *  Created by Basil Bayati on 5/5/08.
 *  Copyright 2008 Basil Bayati. All rights reserved.
 *
 */

#include "Simulation.h"

Simulation::Simulation(SBMLDocument * sbmlDocument)
{
	this->sbmlDocument	= sbmlDocument;
	this->sbmlModel		= sbmlDocument->getModel();
	
	ModelName			= sbmlModel->getName();
	
	speciesValues.resize(sbmlModel->getNumSpecies());
	proposedSpeciesValues.resize(sbmlModel->getNumSpecies());
	proposedSpeciesValues = (ParticleType)0.0;
	loadInitialConditions();

	timeStart[0] = "<stochSim:TimeStart>";
	timeStart[1] = "</stochSim:TimeStart>";
	
	timeEnd  [0] = "<stochSim:TimeEnd>";
	timeEnd  [1] = "</stochSim:TimeEnd>";
	
    storeInterval[0] = "<stochSim:StoreInterval>";
	storeInterval[1] = "</stochSim:StoreInterval>";
	
    epsilon[0]       = "<stochSim:Epsilon>";
	epsilon[1]       = "</stochSim:Epsilon>";
	
    numberOfSamples[0] = "<stochSim:NumberOfSamples>";
	numberOfSamples[1] = "</stochSim:NumberOfSamples>";	
	
	method[0] = "<stochSim:Method>";
	method[1] = "</stochSim:Method>";
	
	theta[0] = "<stochSim:Theta>";
	theta[1] = "</stochSim:Theta>";
	
	sortInterval[0] = "<stochSim:SortInterval>";
	sortInterval[1] = "</stochSim:SortInterval>";
	
	parseAnnotation();
	speciesEnsemble.resize(sbmlModel->getNumSpecies(), StoreInterval+1);
	speciesEnsemble = 0.0;
	
	// create SSMReaction objects
	for (int ir = 0; ir < sbmlModel->getNumReactions(); ++ir)
	{
		Reaction * sbmlReaction = sbmlModel->getReaction(ir);
		SSMReaction * ssmr = new SSMReaction();
			
		KineticLaw * kineticLaw = sbmlReaction->getKineticLaw();
		Parameter * parameter = kineticLaw->getParameter(0);	

		// save the rate
		ssmr->setRate( parameter->getValue() );
		
		// save the reactants
		for (int j = 0; j < sbmlReaction->getNumReactants(); ++j)
		{
			SpeciesReference * speciesReference = sbmlReaction->getReactant(j);
			string speciesName = speciesReference->getSpecies();
			int speciesIndex = getSpeciesIndex(speciesName);
			ssmr->addReactant(speciesIndex);
			
			double stochiometricCoefficient = speciesReference->getStoichiometry();
			ssmr->setLastReactantNu((int)stochiometricCoefficient);
		}
		
		// save the products
		for (int j = 0; j < sbmlReaction->getNumProducts(); ++j)
		{
			SpeciesReference * speciesReference = sbmlReaction->getProduct(j);
			string speciesName = speciesReference->getSpecies();
			int speciesIndex = getSpeciesIndex(speciesName);
			ssmr->addProduct(speciesIndex);
			
			double stochiometricCoefficient = speciesReference->getStoichiometry();
			ssmr->setLastProductNu((int)stochiometricCoefficient);
		}
		
		ssmr->finalizeReaction();
		
		// for testing
		//ssmr->toString();
		ssmReactionList.push_back(ssmr);
	}
	
	// s	species
	// ir	reaction
	// r	reactants
	for (unsigned int s = 0; s < sbmlModel->getNumSpecies(); ++s)
	{
		// allocate a pointer to hold the reactions
		backPointers.push_back( vector<int>() );
		
		// find the reactions in which a particular species is involved
		for (unsigned int ir = 0; ir < sbmlModel->getNumReactions(); ++ir)
		{
			bool isInvolved = false;
			
			SSMReaction * reaction		= ssmReactionList[ir];
			vector <int>  reactants		= reaction->getReactants();
			
			// loop through the reactants of reaction "ir"
			for (unsigned int r = 0; r < reactants.size(); ++r)
			{
				if ( s == reactants[r] )
				{
					isInvolved = true;
					break;
				}
			}
			
			if (isInvolved == true)
			{
				backPointers[s].push_back(ir);
			}
		}
	}
	
	// for testing...
	/*
	cout << "Species back-pointers to reactions" << endl;
	for (unsigned int s = 0; s < sbmlModel->getNumSpecies(); ++s)
	{
		vector <int> & temp = backPointers[s];
		for (unsigned int j = 0; j < temp.size(); ++j)
		{
			cout << temp[j] << " ";
		}
		cout << endl;
	}
	exit(0);
	*/
}

Simulation::Simulation()
{
}

Simulation::~Simulation()
{
	for (int ir = 0; ir < ssmReactionList.size(); ++ir) { delete ssmReactionList[ir]; }
}

Model * Simulation::getSBMLModel()
{
	return this->sbmlModel;
}

int Simulation::getSpeciesIndex(string speciesName)
{
	iter = speciesData.begin();
	iter = speciesData.find(speciesName);
	return iter->second;
}

void Simulation::loadInitialConditions()
{
	for (int i = 0; i < sbmlModel->getNumSpecies(); ++i)
	{
		sbmlSpecies = sbmlModel->getSpecies(i);
		speciesData.insert(pair<string, int>(sbmlSpecies->getName(), i));
		speciesValues(i) = (ParticleType)sbmlSpecies->getInitialAmount();
		proposedSpeciesValues(i) = (ParticleType)sbmlSpecies->getInitialAmount();
	}
}

double Simulation::dFindSubstringAnnotation(string annotation, string s [2])
{
	int f1, f2;
	f1 = annotation.find(s[0]) + s[0].length();
	f2 = annotation.find(s[1]);
	string ret = annotation.substr(f1, f2-f1);
	return std::atof(ret.c_str());
}

string Simulation::sFindSubstringAnnotation(string annotation, string s [2])
{
	int f1, f2;
	f1 = annotation.find(s[0]) + s[0].length();
	f2 = annotation.find(s[1]);
	string ret = annotation.substr(f1, f2-f1);
	return ret;
}

void Simulation::parseAnnotation()
{

	string annotation = (sbmlModel->getAnnotation());
	StartTime		= dFindSubstringAnnotation(annotation,		timeStart);
	EndTime			= dFindSubstringAnnotation(annotation,		timeEnd);
	StoreInterval   = (int)dFindSubstringAnnotation(annotation,	storeInterval);
	Epsilon			= dFindSubstringAnnotation(annotation,		epsilon);
	NumberOfSamples = (int)dFindSubstringAnnotation(annotation,	numberOfSamples);
	
	StochasticSimulationMethod = sFindSubstringAnnotation(annotation, method); 
	
	SortInterval	= (int)dFindSubstringAnnotation(annotation,	sortInterval);
	Theta			= dFindSubstringAnnotation(annotation,	theta);
}



























