
basePath      = '../';
fileNameSSA             = 'Dimerization-SSA--histogram-ssa.txt';
fileNameRLeaping      = 'Dimerization-RLeaping--histogram-rLeaping.txt';

dataSSA             = dlmread([basePath fileNameSSA], '\t');
dataRLeaping      = dlmread([basePath fileNameRLeaping], '\t');
numberOfBins      = 20;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

X2_SSA = dataSSA(:, 2);
X2_RLP = dataRLeaping(:, 2);

[ySSA,xSSA] = hist(X2_SSA, numberOfBins);
[yRLP,xRLP] = hist(X2_RLP, numberOfBins);

ZSSA = trapz(xSSA, ySSA);
ZRLP = trapz(xRLP, yRLP);

ySSA = ySSA ./ ZSSA;
yRLP = yRLP ./ ZRLP;

hold on
bar(xSSA, ySSA, 'r' );
bar(xRLP, yRLP );

legend('SSA','R-Leaping')
hold off

