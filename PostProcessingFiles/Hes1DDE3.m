
function sol = Hes1DDE3
% This is example 3 of D.R. Wille' and C.T.H. Baker,
% DELSOL--a numerical code for the solution of systems
% of delay-differential equations, Appl. Numer. Math., 
% 9 (992) 223-234. 

tau         = 19.7; % delay on P, y(1)
M_initial   = 3;
P_initial   = 100;
alpha_m     = 1;
alpha_p     = 1;
P0          = 100;
h           = 4.1;
mu_m        = 0.029;
mu_p        = 0.031;

% solution from 0 - 12 hours
options = ddeset('RelTol', 1e-10, 'AbsTol', 1e-10);
sol = dde23(@hes13f,[tau],[P_initial; M_initial],[0, 2880 ]);

ddeget(options, 'RelTol');
ddeget(options, 'AbsTol');

% figure
% hold on
% t = linspace(tau, 2880 ,10000); 
% ylag = deval(sol,t - tau);
% plot(0.03 .* ylag(1,:), 'r-'); 
% plot( ylag(2,:), 'k--'); 
% hold off

%figure
%hold on
%plot(sol.x ./ 60, 0.03 .* (sol.y(1,:)) , 'k--');
%plot(sol.x ./ 60, (sol.y(2,:)) , 'r-');
%title('')
%xlabel('time t');
%ylabel('y(t)');
%hold off

%-----------------------------------------------------------------------

function yp = hes13f(t,y,Z)
%EXAM1F  The derivative function for the Example 1 of the DDE Tutorial.
% y(1) = P
% y(2) = M

tau         = 19.7; % delay on P, y(1)
M_initial   = 3;
P_initial   = 100;
alpha_m     = 1;
alpha_p     = 1;
P0          = 100;
h           = 4.1;
mu_m        = 0.029;
mu_p        = 0.031;

ylag1 = Z(:,1);

% yp = [ alpha_p*y(2) - mu_p*y(1) 
%        alpha_m*( 1 / ( 1 + (ylag1(1)/P0)^h ) )*(heaviside(t-19.699)) - mu_m*y(2)              ];

yp = [ alpha_p*y(2) - mu_p*y(1) 
       alpha_m*( 1 / ( 1 + (ylag1(1)/P0)^h ) )*(heaviside(t-19.69999999)) - mu_m*y(2)              ];
   
   
%alpha_m*( 1 / ( 1 + (ylag1(1)/P0))^h ) - mu_m*y(1)