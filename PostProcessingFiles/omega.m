function y = omega ( x, t, nu ) 

y = zeros( 1, length(x) );

for i = 1: length(x)
        y(i) = (x(i) * exp ( - (x(i) * x(i)) / ( 1 + 4*nu*t ) )  )  / (1 + 4*nu*t)^(3/2);
end

end
