% Post processing for the Hes1 Transcription-Translation Delay
% -Basil Bayati
clc
clear all;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
proteinScalingFactor                 = 0.03;
timeScalingFactor                    = 60;

basePath = '../';
baseFolder = '';

fileNames{1} = [baseFolder 'Hes1-Q100-e0.0005-Output.txt'   ]    ;          plotStyles{1} = 'g--o';
fileNames{2} = [baseFolder 'Hes1-Q1000-e0.0005-Output.txt'   ]    ;             plotStyles{2} = 'b-x';
fileNames{3} = [baseFolder 'Hes1-Q10000-e0.0005-Output.txt']    ;             plotStyles{3} = 'r--o';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
N = length(fileNames);
figure
hold on
q = 100;
for i = 1:N
    data = dlmread([basePath fileNames{i}], '\t');
    time = data(:, 1);
    M    = data(:, 2);
    P    = data(:, 3);
    plot(time ./ timeScalingFactor, P .* (proteinScalingFactor/q), plotStyles{i});
    q = q*10;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%plot the DDE solution
sol = Hes1DDE3();
    % y(1,:) = P
    % y(2,:) = M
tint = time;
yint = deval(sol,tint);
DDEplotStyle{1} = 'k-d';
plot(tint ./ timeScalingFactor, proteinScalingFactor .* yint , DDEplotStyle{1});

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
axis([0 12 2 9])
legend('\beta = 100','\beta = 1000','\beta = 10000','DDE')
hold off
