
clear all;

basePath      = '../';
%fileName       = 'DiffusionC100Q100-RLeaping-Output.txt';
fileName       = 'DiffusionC100Q100-SSA-Output.txt';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

data                 = dlmread([basePath fileName], '\t');

numberOfSpecies = 100;
q                     = 100;
T = 25;
nu = 10^(-2);

xMax = 5;

figure
hold on

speciesVector = [1:numberOfSpecies];

h = xMax/numberOfSpecies;
for j = 1:numberOfSpecies
    x(j) = 0.5*h + ((j-1)*h);
end

yExact = omega( x, T, nu );

time = data(:, 1);

for j = 1:length(time)
    clf
    yExact = omega( x, time(j), nu );
    y = data( j, 2:(numberOfSpecies+1)) ./ q;
    plot( x, data( 1, 2:(numberOfSpecies+1)) ./ q, 'k-o', x, y, 'r--o', x, yExact, 'b-' );
    axis([0 xMax 0 0.45])
    legend('Initial Condition','Stochastic Simulation','Analytical Continuum Solution')
    pause(0.01);
end
hold off
