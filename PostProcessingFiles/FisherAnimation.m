
clear all;

basePath      = '../';
fileName       = 'FisherC100Q100-SSA-Output.txt';
%fileName       = 'FisherC100Q100-RLeaping-Output.txt';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data                 = dlmread([basePath fileName], '\t');

numberOfSpecies = 100;
q                     = 100;

figure
hold on
xMax = numberOfSpecies;
speciesVector = [1:numberOfSpecies];

h = 1;
for j = 1:numberOfSpecies
    x(j) = 0.5*h + ((j-1)*h);
end

time = data(:, 1);

for j = 1:length(time)
    clf
    y = data( j, 2:(numberOfSpecies+1)) ./ q;
    z = data( j, (numberOfSpecies+2):(2*numberOfSpecies+1)) ./ q;
    
    plot( x, data( 1, 2:(numberOfSpecies+1)) ./ q, 'k-o', x, y, 'r-o', x, z, 'b-s');
    axis([0 xMax 0 1.3])
    legend('Initial Condition','A','B')
    pause(0.1);
end

hold off
